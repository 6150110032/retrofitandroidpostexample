package com.example.retrofitandroidpostexample;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.IOException;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {

    private static String TAG_RETROFIT_GET_POST = "RETROFIT_GET_POST";
    private EditText userNameEditText = null;
    private EditText passwordEditText = null;
    private EditText emailEditText = null;
    private Button registerButton = null;
    private Button getAllUserButton = null;
    private Button getUserByNameButton = null;
    private ListView userListView = null;
    private ProgressDialog progressDialog = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setTitle("Android Retrofit Get Post Example.");
        initControls();

        /* When register user account button is clicked. */
        registerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showProgressDialog();
                String usernameValue = userNameEditText.getText().toString();
                String passwordValue = passwordEditText.getText().toString();
                String emailValue = emailEditText.getText().toString();

                // 1. Building retrofit object
                Retrofit retrofit = new Retrofit.Builder()
                        .baseUrl(UserManagerInterface.BASE_URL)
                        .addConverterFactory(GsonConverterFactory.create())
                        .build();

                //Create instance for user manager interface and return it.
                UserManagerInterface userManagerInterface =
                        retrofit.create(UserManagerInterface.class);

                // Use default converter factory, so parse response body text to okhttp3.ResponseBody object.
                Call<ResponseBody> call = userManagerInterface.registerUser(usernameValue, passwordValue, emailValue);

                // Create a Callback object, because we do not set JSON converter, so the return object is ResponseBody be default.
                call.enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call,
                                           Response<ResponseBody> response) {
                        hideProgressDialog();
                        StringBuffer messageBuffer = new StringBuffer();
                        int statusCode = response.code();
                        if(statusCode == 200)
                        {
                            try {
                                // Get return string.
                                String returnBodyText = response.body().string();
                                // Because return text is a json format string, so we should parse it manually.
                                Gson gson = new Gson();
                                TypeToken<List<RegisterResponse>> typeToken = new
                                        TypeToken<List<RegisterResponse>>(){};
                                // Get the response data list from JSON string.
                                List<RegisterResponse> registerResponseList =
                                        gson.fromJson(returnBodyText, typeToken.getType());
                                if(registerResponseList!=null &&
                                        registerResponseList.size() > 0) {
                                    RegisterResponse registerResponse =
                                            registerResponseList.get(0);
                                    if (registerResponse.isSuccess()) {

                                        messageBuffer.append(registerResponse.getMessage());
                                    } else {
                                        messageBuffer.append("User register failed.");
                                    }
                                }
                            }catch(IOException ex)
                            {
                                Log.e(TAG_RETROFIT_GET_POST, ex.getMessage());
                            }
                        }else
                        {
                            // If server return error.
                            messageBuffer.append("Server return error code is ");
                            messageBuffer.append(statusCode);
                            messageBuffer.append("\r\n\r\n");
                            messageBuffer.append("Error message is ");
                            messageBuffer.append(response.message());
                        }
                        // Show a Toast message.
                        Toast.makeText(getApplicationContext(),
                                messageBuffer.toString(), Toast.LENGTH_LONG).show();
                    }
                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        hideProgressDialog();
                        Toast.makeText(getApplicationContext(), t.getMessage(),
                                Toast.LENGTH_LONG).show();
                    }
                });
            }
        });
    }

    /* Initialize all UI controls. */
    private void initControls()
    {
        if(userNameEditText==null)
        {
            userNameEditText = (EditText)findViewById(R.id.retrofit_user_name);
        }
        if(passwordEditText==null)
        {
            passwordEditText = (EditText)findViewById(R.id.retrofit_password);
        }
        if(emailEditText==null)
        {
            emailEditText = (EditText)findViewById(R.id.retrofit_email);
        }
        if(registerButton == null)
        {
            registerButton = (Button)findViewById(R.id.retrofit_register_button);
        }
        if(progressDialog == null) {
            progressDialog = new ProgressDialog(MainActivity.this);
        }
    }

    /* Show progress dialog. */
    private void showProgressDialog()
    {
        // Set progress dialog display message.
        progressDialog.setMessage("Please Wait");
        // The progress dialog can not be cancelled.
        progressDialog.setCancelable(false);
        // Show it.
        progressDialog.show();
    }

    /* Hide progress dialog. */
    private void hideProgressDialog()
    {
        // Close it.
        progressDialog.hide();
    }
}